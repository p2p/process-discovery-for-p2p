import base64
import collections
from main.models import OCEL
from pm4py.objects.petri_net.obj import PetriNet as pm4pyPN
from main.utils import get_ocel_as_df, get_cytoscape_pm_from_db, save_cytoscape_pm_to_db
from main.models import OCEL, SelectedPM
from sap_python.main import discover_pm_interactive, discover_pm_PNG
from os import remove, makedirs
import diskcache
cache = diskcache.Cache("./cache")

num_colors = 6
def convert_pm_to_cytoscape(data, graph_style):
    if graph_style == "PN": 
        places_in_cyto = []
        transitions_in_cyto = []
        arcs_in_cyto = []
        double_arcs_on_activity = data['double_arcs_on_activity']
        petri_nets = collections.OrderedDict(data['petri_nets'])
        # index is used to link object type to available colours in style doc 
        for index,object_type in enumerate(petri_nets):    
            (petri_net_data, [bsource], [bsink])= petri_nets[object_type]
            places = petri_net_data.places
            transitions = petri_net_data.transitions
            arcs = petri_net_data.arcs
            # to get unique 'id's we need to use both the object_type and the place_name
            for place in places:
                label = object_type if (place.name == "sink" or place.name == "source") else ""
                label_type = place.name  + " " if (place.name == "sink" or place.name == "source") else "no_label "
                places_in_cyto += [{'data': {'id':object_type + "_" + place.name, 'label': label}, 'classes' : "place " + label_type + "color" + str(index%num_colors)}]
            
            for transition in transitions:
                label = transition.label 
                # activities have globally unique id's, even when multiple subgraphs are connected -> so work with labels, if they exist
                id = transition.label if transition.label else object_type + "_" + transition.name
                label_type = "no_label " if (not label or label == "") else ""         
                transitions_in_cyto += [{'data': {'id': id, 'label': label if label else ""}, 'classes' : "transition " + label_type + "color" + str(index%num_colors)}]
            
            for arc in arcs:
                raw_source = arc.source
                raw_target = arc.target
                if type(raw_target) == pm4pyPN.Place:
                    activity_label = raw_source.label
                    target = object_type + "_" + raw_target.name #target is place, needs special id
                    source = raw_source.label if raw_source.label else object_type + "_" + raw_source.name 
                else:
                    activity_label = raw_target.label
                    source = object_type + "_" + raw_source.name #source is place, needs special id
                    target =  raw_target.label if raw_target.label else object_type + "_" + raw_target.name

                double_arcs = "double_arcs " if activity_label and double_arcs_on_activity[object_type][activity_label] else ""
                arcs_in_cyto += [{'data': {'source': source, 'target': target}, 'classes': double_arcs + "color" + str(index%num_colors) }]
    # order is important, all places/transitions from all sub-graphs have to exist for arcs
    in_cytoscape_format = places_in_cyto  + transitions_in_cyto + arcs_in_cyto
    return in_cytoscape_format


def get_cytoscape_model(ocel_id, pm_style):
    # Already saved in cytoscape format
    if (get_cytoscape_pm_from_db(ocel_id, pm_style)):
        return get_cytoscape_pm_from_db(ocel_id, pm_style)
    
    if (ocel_id not in OCEL.objects.values_list('id', flat=True)):
        print("OCEL not found!")
        return {}
    my_ocel = get_ocel_as_df(ocel_id)
    my_model = collections.OrderedDict(discover_pm_interactive(my_ocel, pm_style))
    in_cytoscape_format = convert_pm_to_cytoscape(my_model, pm_style)
    if (in_cytoscape_format != []):
        save_cytoscape_pm_to_db(ocel_id, in_cytoscape_format, pm_style)
    return in_cytoscape_format

def get_png_source(ocel_id, pm_style):    
    file_name = str(ocel_id) + "_" + pm_style + ".png"
    #if not file_name in cache:
    makedirs("/PNGs", exist_ok=True)
    discover_pm_PNG(get_ocel_as_df(ocel_id), pm_style, "/PNGs/" + file_name)      
    with open("/PNGs/" + file_name, "rb") as f:
        cache.set(file_name,f.read())
    remove("/PNGs/" + file_name)
    return file_name



def reload_view_page():
    selected = SelectedPM.objects.first()
    if not selected:
        return []
    if selected.format == "CYTO":
        return (get_cytoscape_model(selected.ocel_id.id, selected.pm_style), "CYTO", "")
    if selected.format == "PNG":
        png_source = get_png_source(selected.ocel_id.id, selected.pm_style)
        return ([], "PNG", png_source)
    