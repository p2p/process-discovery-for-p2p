#set width for all types, at least for the ones without labels
pattern_graph_stylesheet = [
    {
        'selector': '.color0',
        'style': {
                    'background-color': '#013561',
                    'color':'#FFFFFF',
                    "target-arrow-color" : '#013561',
                    "line-color": "#013561",
        }
    },
    {
        'selector': '.color1',
        'style': {
                    'background-color': '#265726',
                    'color': '#FFFFFF',
                    "target-arrow-color" : '#265726',
                    "line-color": "#265726",

        }
    },
    {
        'selector': '.color2',
        'style': {
                    'background-color': '#186075',
                    'color': '#FFFFFF',
                    "target-arrow-color" : '#186075',
                    "line-color": "#186075"
        }
    },
    {
        'selector': '.color3',
        'style': {
                    'background-color': '#84520b',
                    'color':'#FFFFFF',
                    "target-arrow-color" : '#84520b',
                    "line-color": "#84520b"

        }
    },
    {
        'selector': '.color4',
        'style': {
                    'background-color': '#6e1a18',
                    'color':'#FFFFFF',
                    "target-arrow-color" : '#6e1a18',
                    "line-color": "#6e1a18"
        }
    },
    {
        'selector': '.color5',
        'style': {
                    'background-color': '#008000',
                    'color':'#FFFFFF' ,
                    "target-arrow-color" : '#008000',
                    "line-color": "#008000"
        }
    },
    {
        "selector": 'node',  # For all nodes
        'style': {
            "opacity": 1,
            "label": "data(label)",  # Label of node to display
            "text-halign": "center",
            "text-valign": "center",
            'width': "label",
            'height': 50,
        }
    },
    {
        'selector': '.no_label',
        'style': {
                    'width': 50,
                    'height': 50
        }
    },
    {
        'selector': '.source',
        'style': {
                    'shape': 'ellipse',
                    'width': 180,
                    'border-width': 2,
                    'border-style': "solid",
        }
    },
    {
        'selector': '.sink',
        'style': {
                    'shape': 'ellipse',
                    'width':180,
                    'border-width': 6,
                    'border-style': "double",
        }
    },
    {
        'selector': '.transition',
        'style': {
                    'shape': 'rectangle',
                    'background-color':'#FFFFFF',
                    'color':'#000000',
                    'border-width': 2,
                    'border-style': "solid",

        }
    },
    {
        "selector": 'edge',  # For all edges
        "style": {
            "target-arrow-shape": "triangle",  # Arrow shape
            'arrow-scale': 2,  # Arrow size
            # Default curve-If it is style, the arrow will not be displayed, so specify it
            'curve-style': 'bezier',
            'label': 'data(label)',
            'text-wrap': 'wrap',
        }
    },
    {
        "selector": '.loop',
        "style": {
            'loop-direction': '-90deg',
            'loop-sweep': '-25deg',
            'control-point-step-size': '100'
            # 'target-endpoint': 'outside-to-line',
            # 'source-endpoint': 'outside-to-line',
        }
    },
]