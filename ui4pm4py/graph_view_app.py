import base64
from dash import html, DiskcacheManager, Input, Output
import dash_cytoscape as cyto
from .style import pattern_graph_stylesheet
from django_plotly_dash import DjangoDash
from ui4pm4py.pm_vis_utils import reload_view_page
import diskcache
cache = diskcache.Cache("./cache")
background_callback_manager = DiskcacheManager(cache)


cyto.load_extra_layouts()
app = DjangoDash('graph_view')
app.layout = html.Div([
        cyto.Cytoscape(
        id='cytoscape-graph-view',
        layout={'name': 'cose-bilkent'},
        style={'width': '100%', 'height': '90vh', 'display':'block'},
        minZoom=0.1,
        maxZoom=16,
        stylesheet=pattern_graph_stylesheet,
        elements= []),
        html.Img(id='graph-png', src='', style={'display':'hidden'})
])

@app.callback(
        output = [Output("cytoscape-graph-view", "elements"), Output("cytoscape-graph-view", "style"), Output("graph-png", "src"), Output("graph-png", "style")], 
        inputs = [Input("cytoscape-graph-view", "elements")], 
        background = True, 
        manager = background_callback_manager)
def update_view(elements):
        (new_elements, format, source) = reload_view_page()
        vis_cyto, vis_png, png = '', '', ''
        if format=="CYTO":
                vis_cyto = 'block'
                vis_png = 'none'
                png = ''
        if format=="PNG":
                vis_cyto = 'none'
                vis_png = 'block'
                png = f'data:image/png;base64,{base64.b64encode(cache.get(source)).decode("utf-8")}'
        return new_elements, {'width': '100%', 'height': '90vh', 'display': vis_cyto}, png, {'display':vis_png, 'object-fit': 'contain',"max-width": "100vw"}