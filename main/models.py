from django.db import models
from django.utils.dateparse import parse_date
import pandas as pd
# Create your models here.

class PandaDFModel(models.Model):
    class Meta:
        abstract = True

    # Convert django model back to pd.dataframe
    @classmethod
    def as_dataframe(cls, queryset=None, field_list=None, df_column_names = None):
        if queryset is None:
            queryset = cls.objects.all()
        if field_list is None:
            field_list = [_field.name for _field in cls._meta._get_fields(reverse=False)]

        data = []
        [data.append([obj.serializable_value(column) for column in field_list]) for obj in queryset]

        columns = df_column_names if (df_column_names) else field_list

        df = pd.DataFrame(data, columns=columns)
        return df

class SAPImport(models.Model):
    username = models.CharField(max_length=30)
    password = models.CharField(max_length=30)
    ocel_name = models.CharField(max_length=100, default="my_ocel_name")

class OCEL_entry(PandaDFModel):
    ocel_eid = models.CharField(db_column="ocel:eid", default = "-1", max_length=255)
    ocel_activity = models.CharField(db_column="ocel:activity", default = "no activity provided", max_length=255)
    ocel_timestamp = models.DateTimeField(db_column = "ocel:timestamp", default=parse_date("2000-01-01"))
    ocel_oid = models.CharField(db_column = "ocel:oid", default = "No oid provided", max_length=255)
    ocel_type = models.CharField(db_column = "ocel:type", default = "No type provided",max_length=255)
    parent_ocel = models.ForeignKey('main.OCEL', on_delete=models.CASCADE,  db_column = "parent_ocel", default = "NoParent")   #What ocel does this entry belong to?

class OCEL(PandaDFModel):
    name = models.CharField(default = "myOCEL", max_length = 100)
    def __str__(self):
        return str(self.id) + " - " + self.name

class PMType(models.Model):
    generate_PN = models.BooleanField(default=True)
    generate_DFG = models.BooleanField(default=True)

class SelectedPM(models.Model):
    ocel_id = models.ForeignKey('main.OCEL', on_delete=models.CASCADE,  db_column = "ocel_id", default = "error")
    pm_style = models.CharField(default = "PN", max_length=8)
    format = models.CharField(default = "PNG", max_length=8)

class CytoscapePM(models.Model):
    ocel_id = models.ForeignKey('main.OCEL', on_delete=models.CASCADE,  db_column = "ocel_id", default = "error")
    cytoscape_pm_json = models.JSONField(null=True, blank=True)
    pm_style = models.CharField(default = "PN", max_length=8)