from django.urls import path
from . import views

app_name = "main"   


urlpatterns = [
    path("", views.import_ocel, name="import_ocel"),
    path("import_ocel", views.import_ocel, name="import_ocel"),
    path("upload", views.upload_data, name="upload"),
    path("view_ocel/", views.view_ocel, name="view_ocel"),
    path("view_ocel/<int:selected_ocel_id>", views.view_ocel, name="view_ocel"),
    path("view/", views.view_pm, name="view"),
    path("view/<int:selected_ocel_id>/<str:pm_style>/<str:format>", views.view_pm, name="view"),
    path("docs", views.docs, name="docs"),
]