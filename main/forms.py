from django import forms
from .models import OCEL
from os.path import splitext
# Create your models here.


class SAPImportForm(forms.Form):
    ocel_name = forms.CharField(label='OCEL Name')
    username = forms.CharField(label='Username')
    password = forms.CharField(label='Password', widget=forms.PasswordInput)

class OCELModelSelectorForm(forms.Form):
    CHOICES = [("PN", "Petri Net"), ("DFG", "Directly Follows Graph"),]
    model_style = forms.ChoiceField(widget = forms.RadioSelect,choices=CHOICES, initial="PN")
    format = forms.ChoiceField(widget= forms.RadioSelect,choices=[("PNG", "Static Image"), ("CYTO", "Interactive Graph")], initial="CYTO")
    ocel = forms.ModelChoiceField(queryset=OCEL.objects.all(), widget = forms.Select(attrs = {'class':"OCEL-select", 'onchange': "SelectOCEL(this.value);"}), label="OCEL")

class OCELUploadForm(forms.Form):
    ocel_name = forms.CharField(label='OCEL Name')
    ocel_file = forms.FileField(label = 'OCEL File', widget = forms.FileInput(attrs = {'type': 'file', 'class':'form-control', 'required': True, }))