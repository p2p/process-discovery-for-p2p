from django.test import TestCase
from django.contrib.messages import get_messages



class AuthenticateCredentialErrorTestCase(TestCase): 
    def test_credentials_valid(self):
        response = self.client.post("/import/authenticate", data={"username" : "wrong", "password": "credentials"})
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), "Could not log you in, please check login credentials!")
    