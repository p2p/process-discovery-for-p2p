from django.http import HttpResponseRedirect
from django.shortcuts import render
try:
    from pyrfc import LogonError, CommunicationError
except ImportError:
    print("You do not have pyrfc installed! You can not use the SAP Connector functionality!")
from .forms import OCELModelSelectorForm, SAPImportForm, OCELUploadForm
from django.contrib import messages
from .utils import *
from .models import OCEL, SelectedPM
from sap_python import main
from pm4py import read_ocel
from django.core.files.storage import FileSystemStorage


# Create your views here.
def import_ocel(request):
    if request.method == 'POST':
        form = None
        if request.POST['button'] == "sap_confirm":
            form = SAPImportForm(request.POST)
            if form.is_valid():
                cleaned_form = form.cleaned_data
                try:
                    new_id = save_df_ocel_to_django_model(main.get_relations(cleaned_form["username"], cleaned_form["password"]), cleaned_form["ocel_name"])
                    return HttpResponseRedirect('/view_ocel/' + str(new_id))
                except CommunicationError:
                    messages.error(request, "Could not connect to server! Make sure your IP address is activated: https://remotelogin.sapucc.in.tum.de/")
                except LogonError:
                    messages.error(request, "Could not log you in, please check login credentials!")
                except Exception as e:
                    messages.error(request, e) 
                return HttpResponseRedirect('/import_ocel')
        
        elif request.POST['button'] == "upload_confirm":
            form = OCELUploadForm(request.POST, request.FILES)
            if form.is_valid():
                new_id = ""
                cleaned_form = form.cleaned_data
                upload_file = cleaned_form["ocel_file"]
                name = cleaned_form["ocel_name"]
                try:
                    fs = FileSystemStorage()
                    fs.save(upload_file.name, upload_file)
                    print("saved: ", name) 
                    ocel = read_ocel(fs.path(upload_file.name))
                    new_id = str(save_df_ocel_to_django_model(ocel.relations, name))
                    fs.delete(fs.path(upload_file.name))
                    return HttpResponseRedirect('/view_ocel/' + new_id)
                except Exception as e:
                    messages.error(request, e)
                finally:
                    fs.delete(fs.path(upload_file.name))            
        return HttpResponseRedirect('/import_ocel')
    else:
        sap_form = SAPImportForm()
        upload_form = OCELUploadForm()
        return render(request, "main/import_ocel.html", {'sap_form': sap_form, 'upload_form': upload_form})

def upload_data(request):
    if request.method == "POST":
        form = OCELUploadForm(request.POST, request.FILES)
        if form.is_valid():
            new_id = ""
            cleaned_form = form.cleaned_data
            upload_file = cleaned_form["ocel_file"]
            name = cleaned_form["ocel_name"]
            try:
                fs = FileSystemStorage()
                fs.save(upload_file.name, upload_file)
                print("saved: ", name)
                ocel = read_ocel(fs.path(upload_file.name))
                new_id = str(save_df_ocel_to_django_model(ocel.relations, name))
                fs.delete(fs.path(upload_file.name))
                return HttpResponseRedirect('/view_ocel/' + new_id)
            except Exception as e:
                messages.error(e)                
            return HttpResponseRedirect('/upload/' + new_id)
    else:
        form = OCELUploadForm()
    return render(request = request, template_name = "main/upload.html", context = {'form': form})

def view_ocel(request, selected_ocel_id = -1):
    # Received results of form (OCEL Model Selector)
    if request.method == 'POST':

        if request.POST['button'] == "discover":
            form = OCELModelSelectorForm(request.POST)
            if form.is_valid():
                cleaned_form = form.cleaned_data
                ocel_id = get_id_from_id_name(str(cleaned_form["ocel"]))
                style = cleaned_form["model_style"]
                format = cleaned_form["format"]
                return HttpResponseRedirect('/view/' + str(ocel_id) + "/" + style + "/" + format)

        elif request.POST['button'] == "delete":
            ocelid = request.POST['ocel']
            delete_ocel(ocelid)
            return HttpResponseRedirect('/view_ocel/')
        

    ocel_df = get_all_ocel_entries_as_string_df()
    columns = [{'field': f, 'title': f} for f in ocel_df.columns]
    ocel_data_json = ocel_df.to_json(orient='records')
    context = {
        "ModelSelectorForm": OCELModelSelectorForm(),
        "data": ocel_data_json,
        "columns" : columns, 
        "selectedOCEL": selected_ocel_id,
    }    
    return render(request = request, template_name = "main/view_ocel.html", context=context)

def view_pm(request, selected_ocel_id = -1, pm_style = "", format = ""):
    if selected_ocel_id not in get_ocel_id_name_dict().keys() or pm_style not in ["PN", "DFG"] or format not in ["PNG", "CYTO"]:
        return HttpResponseRedirect('/view_ocel/')
    if pm_style == "DFG" and format == "CYTO":
        return HttpResponseRedirect("/view/" + str(selected_ocel_id) +"/" + pm_style + "/" + "PNG")
    SelectedPM.objects.all().delete()
    selection = SelectedPM(ocel_id = OCEL.objects.filter(id = selected_ocel_id).first(), pm_style = pm_style, format = format)
    selection.save()
    return render(request,'main/view.html')

def docs(request):
    return render(request = request, template_name = "main/docs.html")