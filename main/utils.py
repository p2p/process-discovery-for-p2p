import json
from .models import OCEL, OCEL_entry, CytoscapePM
from sqlalchemy import create_engine

def save_df_ocel_to_django_model(ocel_as_df, ocel_name):
    if ocel_as_df.empty:
        raise RuntimeError("OCEL is empty!")
    engine = create_engine('sqlite:///db.sqlite3')
    # convert df to django model
    newOCEL = OCEL(name = ocel_name)
    newOCEL.save()
    ocel_as_df['parent_ocel'] = newOCEL.id
    # ensure relevant and only the relevant columns are there
    column_names = ocel_as_df.columns.to_list()
    for column_name in column_names:
        if column_name not in [field.db_column for field in OCEL_entry._meta.fields]:
            ocel_as_df.drop(column_name,axis=1, inplace=True)
    ocel_as_df.to_sql(OCEL_entry._meta.db_table, con = engine, if_exists='append', index = False)
    return newOCEL.id

def delete_ocel(ocelid):
    OCEL.objects.filter(id=ocelid).delete()
    # OCEL_entry does not need to be explicitly deleted because of the "on_delete" mode of the foreign key in OCEL_entry

# Used for visual representation of OCEL 
def get_all_ocel_entries_as_df():
    ocel_as_df = OCEL_entry.as_dataframe(field_list=["ocel_eid", "ocel_activity", "ocel_timestamp", "ocel_oid", "ocel_type", "parent_ocel"], df_column_names=["ocel:eid", "ocel:activity", "ocel:timestamp", "ocel:oid", "ocel:type", "OCEL_id_name"])
    ocel_id_name_dict = get_ocel_id_name_dict()
    ocel_as_df['OCEL_id_name'] = ocel_as_df['OCEL_id_name'].map(ocel_id_name_dict) #replace ids with "id - name" for readability
    delete_ocel(1)
    return ocel_as_df

def get_all_ocel_entries_as_string_df():
    return get_all_ocel_entries_as_df().astype(str)

# Used for PM4Py
def get_ocel_as_df(ocel_id):
    all_ocel_entries = OCEL_entry.as_dataframe(field_list=["ocel_eid", "ocel_activity", "ocel_timestamp", "ocel_oid", "ocel_type", "parent_ocel"], df_column_names=["ocel:eid", "ocel:activity", "ocel:timestamp", "ocel:oid", "ocel:type", "parent_ocel"])
    relevant_ocel_entries = all_ocel_entries[all_ocel_entries['parent_ocel'] == ocel_id]
    return relevant_ocel_entries.filter(items=["ocel:eid", "ocel:activity", "ocel:timestamp", "ocel:oid", "ocel:type"])

def get_list_of_ocels_as_df():
    return OCEL.as_dataframe(field_list=["id", "name"], df_column_names=["#", "Name"])

def get_list_of_ocels_as_string_df():
    return get_list_of_ocels_as_df().astype(str)

def get_ocel_id_names():
    return [str(ocel) for ocel in OCEL.objects.all()]

def get_id_from_id_name(id_name):
    return id_name.split(" ")[0]

def get_ocel_id_name_dict():
    return {i: str(OCEL.objects.filter(id=i)[0].id) + " - " +OCEL.objects.filter(id=i)[0].name for i in OCEL.objects.values_list('id', flat = True)}

def get_cytoscape_pm_from_db(ocel_id, pm_style):
    model = CytoscapePM.objects.filter(ocel_id=ocel_id, pm_style=pm_style).first()
    if model:
        return json.loads(model.cytoscape_pm_json)
    return None

def save_cytoscape_pm_to_db(ocel_id, cytoscape_pm, pm_style):
    # Already exists
    if get_cytoscape_pm_from_db(ocel_id, pm_style):
        return
    object = CytoscapePM(ocel_id = OCEL.objects.filter(id=ocel_id).first(), cytoscape_pm_json = json.dumps(cytoscape_pm), pm_style = pm_style)
    object.save()