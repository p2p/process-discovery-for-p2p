# Process Discovery For P2P

## Getting started with Django and venv in this project
1. It is recommended to use a [virtual environment](https://docs.python.org/3/library/venv.html), this has to be set up **before cloning the repository**
   1. Run ``py -m venv <PATH TO A NEW FOLDER>``, this will create a virtual environment in that folder
   2. Clone the git repo into a (new) folder within the virtual environment folder
   3. To activate the virtual environment call ``.\Scripts\activate.bat`` from the virtual environment folder (this has to be done whenever you want to use the venv)
2. (in activated virtual environment) Install django: ``py -m pip install django==4.1.3`` 
3. (in activated virtual environment) Within the main folder of the repo, run ``py manage.py runserver`` to start the server, the website will then be accessible at ``localhost:8000``