from sap_python.sap_call import SapCall
import sap_python.ocel_generator as ocelgen
import pm4py

def get_relations(username, password):
    sap_call = SapCall()
    sap_call.login(username,password)
    tables = sap_call.retrieve_data()
    relations = ocelgen.generate_relations(tables)
    sap_call.logout()
    return relations

def discover_pm_interactive(ocel_data,graph_style):
    ocel_data = ocelgen.ocel_from_relations(ocel_data)
    try:
        if graph_style=='PN': 
            return pm4py.discover_oc_petri_net(ocel_data)
        elif graph_style=='DFG':
            return pm4py.discover_ocdfg(ocel_data)    
    except:
        print("Can't generate a model given the passed parameters!")
        raise

def discover_pm_PNG(ocel_data, graph_style, path_to_file):
    ocel_data = ocelgen.ocel_from_relations(ocel_data)
    try: 
        if graph_style =="PN":
            pm4py.save_vis_ocpn(pm4py.discover_oc_petri_net(ocel_data), file_path=path_to_file)
        if graph_style == "DFG":
            pm4py.save_vis_ocdfg(pm4py.discover_ocdfg(ocel_data), file_path=path_to_file)
    except:
        print("Can't generate a model given the passed parameters!")
        raise