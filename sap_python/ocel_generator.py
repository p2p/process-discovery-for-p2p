import pm4py
import pandas as pd

def turn_object_into_ocel_entry(df, activity, type, offset):
    ocel = pm4py.objects.ocel.obj.OCEL()
    timeformat = '%Y%m%d%H%M%S'
    if 'Time' in df.columns:
        ocel.relations['ocel:timestamp'] = pd.to_datetime(df['Date'] + df['Time'], format=timeformat)
    else:
        ocel.relations['ocel:timestamp'] = pd.to_datetime(df['Date'] + '000000', format=timeformat)
    ocel.relations['ocel:eid'] = ocel.relations.index + offset
    ocel.relations['ocel:activity'] = activity
    ocel.relations['ocel:type'] = type
    ocel.relations['ocel:oid'] = df[type]
    return ocel

def turn_dataframe_into_relations(df, activity, types, offset=0):
    relations = pd.DataFrame.empty
    ocels = []

    for type in types:
        ocels.append(turn_object_into_ocel_entry(df, activity, type, offset).relations)

    relations = pd.concat(ocels, ignore_index=True)
    relations = relations.sort_values(by='ocel:eid')

    return relations

def ocel_from_relations(relations):
    ocel = pm4py.objects.ocel.obj.OCEL()
    ocel.relations = relations
    ocel.events['ocel:timestamp'] = relations['ocel:timestamp']
    ocel.events['ocel:activity'] = relations['ocel:activity']
    ocel.events['ocel:eid'] = relations['ocel:eid']

    ocel.objects['ocel:type'] = relations['ocel:type']
    ocel.objects['ocel:oid'] = relations['ocel:oid']

    return ocel

def split_dataframe_by_unique_row_value(df, row):
    list_of_dfs = []
    while not df.empty:
        condition = df[row] == df[row][0]
        list_of_dfs.append(df[condition])
        df = df[~condition]
        df = df.reset_index(drop=True)

    return list_of_dfs

def merge_split_dataframe_into_relation(list_of_dfs, activity, types, offset):
    relations = []
    for index, df in enumerate(list_of_dfs):
        relation = turn_dataframe_into_relations(df, activity, types, offset=offset+index)
        relation = relation.sort_values(by='ocel:timestamp')
        relations.append(relation.iloc[0])

    return pd.DataFrame(relations)

def generate_relations(tables):
    purchase_requisition = turn_dataframe_into_relations(tables['Purchase Requisition'], activity='Create Purchase Requisition', types=['Purchase Requisition'])
    offset = purchase_requisition['ocel:eid'].iloc[-1] + 1

    request_for_quotation = turn_dataframe_into_relations(tables['Request for Quotation'], activity='Create Request for Quotation', types=['Quotation', 'Purchase Requisition'], offset=offset)
    offset = request_for_quotation['ocel:eid'].iloc[-1] + 1

    maintain_quotation_split = split_dataframe_by_unique_row_value(tables['Maintain Quotation'], 'Quotation')
    maintain_quotation = merge_split_dataframe_into_relation(maintain_quotation_split, activity='Maintain Quotation', types=['Quotation', 'Purchase Requisition'], offset=offset)
    offset = maintain_quotation['ocel:eid'].iloc[-1] + 1

    condition = tables['Maintain Quotation']['ABSKZ'] == 'X'
    rejected_quotation = tables['Maintain Quotation'][condition]
    rejected_quotation_split = split_dataframe_by_unique_row_value(rejected_quotation, 'Quotation')
    reject_quotation = merge_split_dataframe_into_relation(rejected_quotation_split, activity='Reject quotation', types=['Quotation', 'Purchase Requisition'], offset=offset)
    offset = reject_quotation['ocel:eid'].iloc[-1] + 1

    purchase_order = turn_dataframe_into_relations(tables['Purchase Order'], activity='Create Purchase Order', types=['Purchase Document', 'Purchase Requisition', 'Quotation'], offset=offset)
    offset = purchase_order['ocel:eid'].iloc[-1] + 1

    goods_receipt = turn_dataframe_into_relations(tables['Goods Receipt'], activity='Goods Receipt', types=['Purchase Document', 'Accounting Document'], offset=offset)
    offset = goods_receipt['ocel:eid'].iloc[-1] + 1
    
    invoice_receipt = turn_dataframe_into_relations(tables['Invoice Receipt'], activity='Invoice Receipt', types=['Purchase Document', 'Accounting Document'], offset=offset)
    offset = goods_receipt['ocel:eid'].iloc[-1] + 1
    
    post_payments = turn_dataframe_into_relations(tables['Post Payments'], activity='Post Payments to Vendor', types=['Accounting Document'], offset=offset)

    relations = [
        purchase_requisition.drop_duplicates(subset=['ocel:activity','ocel:oid','ocel:type'], ignore_index=True),
        request_for_quotation.drop_duplicates(subset=['ocel:activity','ocel:oid','ocel:type'], ignore_index=True),
        maintain_quotation.drop_duplicates(subset=['ocel:activity','ocel:oid','ocel:type'], ignore_index=True),
        reject_quotation.drop_duplicates(subset=['ocel:activity','ocel:oid','ocel:type'], ignore_index=True),
        purchase_order.drop_duplicates(subset=['ocel:activity','ocel:oid','ocel:type'], ignore_index=True),
        goods_receipt.drop_duplicates(subset=['ocel:activity','ocel:oid','ocel:type'], ignore_index=True),
        invoice_receipt.drop_duplicates(subset=['ocel:activity','ocel:oid','ocel:type'], ignore_index=True),
        post_payments
    ]
    relations = pd.concat(relations, ignore_index=True)
    relations['ocel:eid'] = relations.index
    relations = relations.drop(relations[relations['ocel:oid'] == ''].index)

    return relations