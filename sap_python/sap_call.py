from sap_python.sap_con import SapConnector
from sap_python import constants

from sap_python.login_error import LoginError
import pandas as pd
import numpy as np

class SapCall:

    sap_con = None

    def login(self, username, password):
        if self.sap_con == None:
            try: 
                self.con_params = constants.sap_conn_params
                self.con_params['user'] = username
                self.con_params['passwd'] = password
                self.sap_con = SapConnector.getInstance(constants.sap_conn_params)
                con_details = self.sap_con.get_con_details()
                print(con_details)
            except:
                raise
        else:
            raise LoginError("Some user is already logged in. Log out to proceed.")

    def logout(self):
        if self.sap_con is not None:
            self.sap_con.close_instance()
        self.sap_con = None
        self.con_params = None

    def executeQuery(self, query=constants.tables, options=''):
        results = []
        headers = []
        for table, fields in query.items():
            result, header = self.sap_con.qry(
                fields, table, MaxRows=constants.ROWS_AT_A_TIME, Where=options)
            results.append(result)
            headers.append(header)
        return headers[0], results[0]

    def to_dataframe(self, header, table):
        df = pd.DataFrame(table)
        df.columns = header
        return df

    def query_by_IN_condition(self, query, fieldname, values):
        body = []
        for entry in values:
            options = [f"{fieldname} = '{entry}'"]
            header, row = self.executeQuery(query,options)
            body.append(row[0])
        return self.to_dataframe(header, body)

    def query(self, query, options=""):
        header, table = self.executeQuery(query,options=options)
        return self.to_dataframe(header, table)

    """ removes whitespace from table entries """
    def sanitize_data(self, data, drop_empty_string=False):
        data = pd.DataFrame(data)
        data = data.apply(lambda x: x.str.strip())
        if drop_empty_string:
            data = data.replace('', np.nan).dropna()
        return data

    def get_column(self, frame, index):
        return frame.loc[:, index].drop_duplicates().str.strip()

    """ table1 is extended by the values of table2 if they have matching 
        values in the column specified by on1 and on2 """
    def join_tables(self, table1, on1, table2, on2, how='inner'):
        table2 = table2.rename(columns={on2:on1})
        table1 = self.sanitize_data(table1)
        table2 = self.sanitize_data(table2)

        return table1.merge(table2, how=how,on=on1)

    def retrieve_data(self):

        """ Get data regarding the "Create Purchase Requisition" process """
        
        eban = self.query(constants.queries['EBAN'])
        eban = eban.rename(columns = constants.renames) 
        purchase_requisition = self.sanitize_data(eban, True)

        """ Get data regarding the "Create Request for Quotation" process """
        
        options = ["TCODE = 'ME41'"]
        cdhdr = self.query(constants.queries['CDHDR'],options=options)

        ekko = self.query_by_IN_condition(constants.queries['EKKO'], 'EBELN', self.get_column(cdhdr, 'OBJECTID'))
        ekko = self.join_tables(ekko, 'EBELN', cdhdr, 'OBJECTID')

        eket = self.query_by_IN_condition(constants.queries['EKET'], 'EBELN', self.get_column(ekko, 'EBELN'))
        eket = self.join_tables(ekko, 'EBELN', eket, 'EBELN')
        request_for_quotation = eket.rename(columns = constants.renames)
        request_for_quotation = request_for_quotation.rename(columns={
            'Purchase Document' : 'Quotation',
        })

        """ Get data regarding the "Maintain Quotations from Vendors" process """
        options = ["TCODE = 'ME47'"]
        cdhdr = self.query(constants.queries['CDHDR'],options)

        a016 = self.query(constants.queries['A016'])
        a016 = self.join_tables(a016, 'EVRTN', cdhdr, 'OBJECTID')

        ekpo = self.query(constants.queries['EKPO'])
        a016 = self.join_tables(a016, 'EVRTN', ekpo, 'EBELN')

        maintain_quotation = a016.rename(columns = constants.renames)
        maintain_quotation = maintain_quotation.rename(columns={
            'Purchase Document' : 'Quotation'
        })

        """ Get data regarding the "Create Purchase Order Referencing an RFQ" process """
        
        options = ["TCODE = 'ME21N'"]
        cdhdr = self.query(constants.queries['CDHDR'],options)

        ekko = self.query(constants.queries['EKKO'])
        ekko = self.join_tables(ekko, 'EBELN', cdhdr, 'OBJECTID')

        ekpo = self.query(constants.queries['EKPO'])
        ekpo = self.join_tables(ekpo, 'EBELN', ekko, 'EBELN')

        eket = self.query(constants.queries['EKET'])
        eket = self.join_tables(eket, 'EBELN', maintain_quotation, 'Quotation')
        eket = eket.drop(eket[eket['ABSKZ'] == 'X'].index)
        eket = eket.rename(columns={'EBELN':'Quotation'})
        eket = eket.drop(['Date', 'Time', 'ABSKZ', 'Purchase Requisition'], axis=1)
        ekpo = self.join_tables(ekpo, 'BANFN', eket, 'BANFN', how='left')
        ekpo = ekpo.fillna('')
        purchase_order = ekpo.rename(columns=constants.renames)

        """ Get data regarding the "Create Goods Receipt for Purchase Order" process """

        options = ["TCODE = 'MIGO_GR'"]
        bkpf = self.query(constants.queries['BKPF'],options)
        bkpf['AWKEY'] = [x[:-4] for x in bkpf['AWKEY']]

        ekbe = self.query(constants.queries['EKBE'])
        bkpf = self.join_tables(bkpf, 'AWKEY', ekbe, 'BELNR')

        goods_receipt = bkpf.rename(columns=constants.renames)

        """ Get data regarding the "Create Invoice Receipt from Vendor" process """

        options = ["TCODE = 'MIRO'"]
        bkpf = self.query(constants.queries['BKPF'],options)
        ekbe = self.query(constants.queries['EKBE'])
        bkpf = self.join_tables(bkpf, 'BELNR', ekbe, 'BELNR')

        invoice_receipt = bkpf.rename(columns=constants.renames)

        """ Get data regarding the "Post Payments to Vendor" process """

        options = ["TCODE = 'FBZ2'"]
        bkpf = self.query(constants.queries['BKPF'],options)
        post_payments = bkpf.rename(columns=constants.renames)

        tables = {
            'Purchase Requisition': purchase_requisition,
            'Request for Quotation': request_for_quotation,
            'Maintain Quotation' : maintain_quotation,
            'Purchase Order': purchase_order,
            'Goods Receipt' : goods_receipt,
            'Invoice Receipt' : invoice_receipt,
            'Post Payments' : post_payments
        }

        return tables
    