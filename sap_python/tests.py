from django.test import TestCase
from .sap_con import SapConnector
from .sap_call import SapCall
from pyrfc import RFCError
from .login_error import LoginError

class SapPyTestCase(TestCase):
    def setUp(self):
        self.sap_call = SapCall()

    """All other tests are dependent on this one because the SAPConnector 
    Instance is created in such a way that it cannot be "manually" restarted"""
    def test_login_out_in(self):
        self.sap_call.login('konrad','testtest')
        self.sap_call.logout()
        try:
            self.sap_call.login('konrad','testtest')
        except RFCError:
            self.fail("Logging in after logging out raised an exception")

    def test_double_login(self):
        self.sap_call.login('konrad','testtest')
        self.assertRaises(LoginError, self.sap_call.login, 'konrad', 'testtest')

