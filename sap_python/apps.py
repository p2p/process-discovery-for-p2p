from django.apps import AppConfig

class SapPyConfig(AppConfig):
    name = 'sap_python'